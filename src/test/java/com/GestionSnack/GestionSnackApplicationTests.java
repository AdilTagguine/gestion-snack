package com.GestionSnack;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.Entities.Client;
import com.Entities.Commande;
import com.Entities.Produit;
import com.dao.ClientRepository;
import com.dao.CommandeRepository;
import com.dao.ProduitRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GestionSnackApplicationTests implements CommandLineRunner{

	@Autowired
    private ClientRepository clientRepository ;
	
	@Autowired
    private CommandeRepository commandeRepository ;
	
	@Autowired
    private ProduitRepository produitRepository ;
  
    
    
    
	public static void main(String[] args) {
		SpringApplication.run(GestionSnackApplicationTests.class, args);
	}
	@Override
	public void run(String... arg0) throws Exception {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		// Décommenter le bloc au-dessous, exécuter l'application une seule fois, vérifier la bdd si les profils sont crées, recommenter le bloc
		
		Client admin = new Client("TAGGUINE", "adil.tagguine@gmail.com", 670682364);
		clientRepository.save(admin);
		
		Commande commande = new Commande("Hssan 10000 Rabat MAROC");
		commandeRepository.save(commande);
		
		Produit produit = new Produit("Costume", 1000, "costume.jpg", "Noir bonne qualité");
		produitRepository.save(produit);
	
		
	}
	
}
