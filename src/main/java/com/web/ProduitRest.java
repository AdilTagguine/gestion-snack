package com.web;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.Entities.Produit;
import com.dao.ProduitRepository;

@RestController
@CrossOrigin("*")
public class ProduitRest {

	@Autowired
	private ProduitRepository produitRepository;
	
	@RequestMapping(value="/produit", method=RequestMethod.GET)
	public List<Produit> getAllProduits(){
		return produitRepository.findAll();
	}
	
	
	@RequestMapping(value="/produit/{id_produit}", method=RequestMethod.GET)
	public Optional<Produit> getProduit(@PathVariable int id_produit){
		return produitRepository.findById(id_produit);
	}
	
	
	@RequestMapping(value="/produit", method=RequestMethod.POST)
	public Produit save(@RequestBody Produit p){
		return produitRepository.save(p);
	}
	
	@RequestMapping(value="/produit/{id_produit}", method=RequestMethod.DELETE)
	public boolean supprimer(@PathVariable int id_produit){
		 produitRepository.deleteById(id_produit);
		 return true;
	}
	
	@RequestMapping(value="/produit/{id_produit}", method=RequestMethod.PUT)
	public Produit update(@PathVariable long id_produit,@RequestBody Produit p){
		p.setId_produit(id_produit);
		return produitRepository.save(p);
	}
	
	@RequestMapping(value="/chercherproduit", method=RequestMethod.GET)
	public Page<Produit> chercher(
			@RequestParam(name="mc",defaultValue="") String mc,
			@RequestParam(name="page",defaultValue="0")int page,
			@RequestParam(name="size",defaultValue="5")int size){
			
		return produitRepository.chercher("%"+mc+"%", new PageRequest(page, size));
	}
}

