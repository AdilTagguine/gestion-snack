package com.web;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.Entities.Commande;
import com.dao.CommandeRepository;

@RestController
@CrossOrigin("*")
public class CommandeRest {


	@Autowired
	private CommandeRepository commandeRepository;
	
	@RequestMapping(value="/commande", method=RequestMethod.GET)
	public List<Commande> getAllCommandes(){
		return commandeRepository.findAll();
	}
	
	
	@RequestMapping(value="/commande/{id_Commande}", method=RequestMethod.GET)
	public Optional<Commande> getCommande(@PathVariable int id_Commande){
		return commandeRepository.findById(id_Commande);
	}
	
	
	@RequestMapping(value="/commande", method=RequestMethod.POST)
	public Commande save(@RequestBody Commande p){
		return commandeRepository.save(p);
	}
	
	@RequestMapping(value="/commande/{id_Commande}", method=RequestMethod.DELETE)
	public boolean supprimer(@PathVariable int id_Commande){
		commandeRepository.deleteById(id_Commande);
		 return true;
	}
	
	@RequestMapping(value="/client/{id_Commande}", method=RequestMethod.PUT)
	public Commande update(@PathVariable long id_Commande,@RequestBody Commande p){
		p.setId_Commande(id_Commande);
		return commandeRepository.save(p);
	}
	
	@RequestMapping(value="/RechercheCommande", method=RequestMethod.GET)
	public Page<Commande> chercher(
			@RequestParam(name="mc",defaultValue="") String mc,
			@RequestParam(name="page",defaultValue="0")int page,
			@RequestParam(name="size",defaultValue="5")int size){
			
		return commandeRepository.chercher("%"+mc+"%", new PageRequest(page, size));
	}
}





