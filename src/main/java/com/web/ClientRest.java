package com.web;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.Entities.Client;
import com.dao.ClientRepository;

@RestController
@CrossOrigin("*")
public class ClientRest {

	@Autowired
	private ClientRepository clientRepository;
	
	@RequestMapping(value="/client", method=RequestMethod.GET)
	public List<Client> getAllClients(){
		return clientRepository.findAll();
	}
	
	
	@RequestMapping(value="/client/{id_client}", method=RequestMethod.GET)
	public Optional<Client> getClient(@PathVariable int id_client){
		return clientRepository.findById(id_client);
	}
	
	
	@RequestMapping(value="/produit", method=RequestMethod.POST)
	public Client save(@RequestBody Client p){
		return clientRepository.save(p);
	}
	
	@RequestMapping(value="/client/{id_client}", method=RequestMethod.DELETE)
	public boolean supprimer(@PathVariable int id_client){
		clientRepository.deleteById(id_client);
		 return true;
	}
	
	@RequestMapping(value="/client/{id_client}", method=RequestMethod.PUT)
	public Client update(@PathVariable long id_client,@RequestBody Client p){
		p.setId_client(id_client);
		return clientRepository.save(p);
	}
	
	@RequestMapping(value="/chercherclient", method=RequestMethod.GET)
	public Page<Client> chercher(
			@RequestParam(name="mc",defaultValue="") String mc,
			@RequestParam(name="page",defaultValue="0")int page,
			@RequestParam(name="size",defaultValue="5")int size){
			
		return clientRepository.chercher("%"+mc+"%", new PageRequest(page, size));
	}
}


