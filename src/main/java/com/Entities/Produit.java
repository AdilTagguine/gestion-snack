package com.Entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Produit {
	
	@Id @GeneratedValue
	private Long id_produit;
	private String nom;
	private double prix;
	private String photo;
	private String description;
	
	
	public Produit() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Produit(String nom, double prix, String photo, String description) {
		super();
		this.nom = nom;
		this.prix = prix;
		this.photo = photo;
		this.description = description;
	}


	public Long getId_produit() {
		return id_produit;
	}


	public void setId_produit(Long id_produit) {
		this.id_produit = id_produit;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public double getPrix() {
		return prix;
	}


	public void setPrix(double prix) {
		this.prix = prix;
	}


	public String getPhoto() {
		return photo;
	}


	public void setPhoto(String photo) {
		this.photo = photo;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	
}
