package com.Entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class Commande {
	@Id @GeneratedValue
	private Long id_Commande;
	private String adresse;
	
	
	private List<Produit> produit = new ArrayList<Produit>();
	
	public Commande() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Commande(String adresse) {
		super();
		
		this.adresse = adresse;
	}

	public Long getId_Commande() {
		return id_Commande;
	}

	public void setId_Commande(Long id_Commande) {
		this.id_Commande = id_Commande;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public List<Produit> getProduit() {
		return produit;
	}

	public void setProduit(List<Produit> produit) {
		this.produit = produit;
	}
	
	
	
}
