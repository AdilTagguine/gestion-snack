package com.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import com.Entities.Produit;

public interface ProduitRepository extends JpaRepository<Produit, Integer>{
	
	@Query("select p from Produit p where p.id like :x")
	public Page<Produit> chercher(@Param("x")String mc,Pageable pageable);

}
